# Image digest resolved by running:
#   docker pull docker.io/library/debian:10-slim on Mon Feb 5 11:22:04 EST 2024
FROM docker.io/library/debian@sha256:3ab507bedfd2e44b5329130c094953111aa701c264262e0432e4f78b40eb99d8

# All versions of these packages are vulnerable to one of the following advisories on Debian 10 (buster)
#
# CVE-2020-36773 | ghostscript         | debian      | 10
# CVE-2023-31617 | virtuoso-opensource | debian      | 10
# CVE-2023-46838 | linux               | debian      | 10
# CVE-2023-52389 | poco                | debian      | 10
# CVE-2024-0564  | linux               | debian      | 10
# CVE-2024-0690  | ansible             | debian      | 10
# CVE-2024-0727  | openssl             | debian      | 10
# CVE-2024-0841  | linux               | debian      | 10
# CVE-2024-0985  | postgresql-11       | debian      | 10
# CVE-2024-1019  | modsecurity         | debian      | 10
# CVE-2024-1086  | linux               | debian      | 10
# CVE-2024-21803 | linux               | debian      | 10
# CVE-2024-22667 | vim                 | debian      | 10
# CVE-2024-24806 | libuv1              | debian      | 10
#
RUN apt-get update && \
apt-get install --yes --no-install-suggests --no-install-recommends \
  ansible \
  ghostscript \
  libmodsecurity3 \
  libpoco-dev \
  libuv1 \
  openssl \
  postgresql-11 \
  vim \
  virtuoso-opensource
